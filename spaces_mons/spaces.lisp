(defpackage :shardquest-spaces  ; main package
	(:nicknames :M)
		;; due to the influence of glitch pokémon on this sphere I just can't not take the opportunity to use the symbol 'M
	(:shadowing-import-from :grevillea
		#:printl)
	(:shadowing-import-from :xylem
		#:xylem-value)
	(:shadowing-import-from :shardquest-strain
		#:make-strain)
	(:shadowing-import-from :shardquest-item
		#:make-item)
	(:shadowing-import-from :shardquest-plugin
		#:register-portal-item #:register-species
		)
	(:use :common-lisp :shardquest-spaces.shard)
	(:export
		;; shard package
		#:item-shard-green
		
		;; species
		
		#:faun
		#:faun-baby #:faun-strong #:faun-keeper #:faun-hero
			#:faun-spirit
		
		#:catrix
		#:catrix-chick #:catrix-basic #:catrix-boat
			#:catrix-sun #:catrix-moon
		
		#:killdeer
		#:killdeer-plover
		#:killdeer-parhelion #:killdeer-aphelion  ; tentative, may be moved to another strain
		
		;; portal items
		#:item-pear-common
		#:item-rock
		#:item-number #:item-number-natural #:item-number-complex
		
		;; other items
		#:item-pear-super #:item-pear-mythic
		#:item-number-imaginary #:item-number-infinite
		;; #::item-activator-lead
))
(in-package :shardquest-spaces)


;;; species {{{

;; Fawna -...-> Dheero {{{

(defun faun ()
	(make-strain :name "Fawna"
		:tags (list 'element-blue)
		)
)

(defun faun-baby ()  ; stage 1: Fawna
	(make-strain :name "Fawna" :group 'faun
		:tags (list 'element-blue)
		:paths nil)
)

(defun faun-strong ()  ; stage 2: Guarden
	(make-strain :name "Guarden" :group 'faun
		:tags (list 'element-blue)
		:paths (list 'faun-baby))
)

(defun faun-keeper ()  ; stage 3: sentraur
	(make-strain :name "Sentraur" :group 'faun
		:tags (list 'element-blue)
		:paths (list 'faun-strong))
)

(defun faun-hero ()  ; stage 4a: Dheero
	(make-strain :name "Dheero" :group 'faun
		:tags (list 'element-blue)
		:paths (list 'faun-keeper))
)

(defun faun-spirit ()  ; stage 4b: Ausdeer
	(make-strain :name "Ausdeer" :group 'faun
		:tags (list 'element-blue)
		:paths (list 'faun-keeper))
)

;; }}}}

;; keytstorn/"kaginari" {{{

(defun kaminari ()
	(make-strain :name "Keytstorn"
		:tags (list 'element-red 'element-blue)
		)
)

(defun kaminari-basic ()  ; stage 2: Keytstorn
	(make-strain :name "Keytstorn" :group 'kaminari
		:tags (list 'element-red 'element-blue)
		:paths nil)
)

(defun kaminari-arch ()  ; stage 4
	(make-strain :name "Higherarckey" :group 'kaminari
		:tags (list 'element-red 'element-blue)
		:paths 'kaminari-basic)
)
;; the other stages are unfinished for now

;; }}}

;; Owbell -...-> Owlfernaut {{{

(defun owbell ()
	(make-strain :name "Owbell"
		:tags (list 'element-blue)
		)
)

(defun owbell-basic ()  ; stage 1: Owbell
	(make-strain :name "Owbell" :group 'owbell
		:tags (list 'element-blue)
		:paths nil)
)

(defun owbell-turn ()  ; stage 2: Jumbowll
	(make-strain :name "Jumbowll" :group 'owbell
		:tags (list 'element-blue)
		:paths 'owbell-basic)
)

(defun owbell-shell ()  ; stage 3: Ownwownd
	(make-strain :name "Ownwownd" :group 'owbell
		:tags (list 'element-blue)
		:paths 'owbell-turn)
)

(defun owbell-ruin ()  ; stage 4: Owlfernaut
	(make-strain :name "Owlfernaut" :group 'owbell
		:tags (list 'element-blue)
		:paths 'owbell-shell)
)

;; }}}

;; plant {{{

(defun lyre ()
	(make-strain :name "lyre"
		:tags (list 'element-blue)
		)
)

(defun lyre-basic ()  ; stage 1
	(make-strain :name "lyre" :group 'lyre
		:tags (list 'element-blue)
		:paths nil)
)

(defun lyre-heart ()  ; stage V
	(make-strain :name "Bleeding Heart" :group 'lyre
		:tags (list 'element-blue)
		:paths nil)
)

;; }}}

;; Chíllín {{{

(defun icecream ()
	(make-strain :name "Chíllín"
		:tags (list 'element-blue)
		)
)

(defun icecream-basic ()  ; stage 2
	(make-strain :name "Chíllín" :group 'icecream
		:tags (list 'element-blue)
		:paths nil)
)

(defun icecream-tidy ()  ; stage 3
	(make-strain :name "zhěngqílín" :group 'icecream
		:tags (list 'element-blue)
		:paths 'icecream-basic)
)

(defun icecream-scenic ()  ; stage 4
	(make-strain :name "Majesqí" :group 'icecream
		:tags (list 'element-blue)
		:paths 'icecream-tidy)
)

;; }}}

;; Catrix -...-> Plodestone -> Sdawne/Muyyn {{{

(defun catrix ()
	(make-strain :name "Catrix"
		:tags (list 'element-red 'element-blue)
		)
)

(defun catrix-chick ()  ; stage 1: Kittrick
	(make-strain :name "Kittrick" :group 'catrix
		:tags (list 'element-red 'element-blue)
		:paths nil)
)

(defun catrix-basic ()  ; stage 2: Catrix
	(make-strain :name "Catrix" :group 'catrix
		:tags (list 'element-red 'element-blue)
		:paths 'catrix-chick)
)

(defun catrix-boat ()  ; stage 3: Plodestone
	(make-strain :name "Plodestone" :group 'catrix
		:tags (list 'element-red 'element-blue)
		:paths 'catrix-basic)
)

(defun catrix-sun ()  ; stage 4a: Sdawne
	(make-strain :name "Sdawne" :group 'catrix
		:tags (list 'element-red 'element-blue)
		:paths 'catrix-boat)
)

(defun catrix-moon ()  ; stage 4b: Muyyn
	(make-strain :name "Muyyn" :group 'catrix
		:tags (list 'element-red 'element-blue)
		:paths 'catrix-boat)
)

;; }}}

;; Ibizaor -...-> Pejustice S/B -> Ibizsedan {{{

(defun dogasus ()
	(make-strain :name "Ibizaor"
		:tags (list 'element-red 'element-blue)
		)
)

(defun dogasus-pup ()  ; stage 1: Minizan
	(make-strain :name "Minizan" :group 'dogasus
		:tags (list 'element-red 'element-blue)
		:paths nil)
)

(defun dogasus-basic ()  ; stage 2: Ibizaor
	(make-strain :name "Ibizaor" :group 'dogasus
		:tags (list 'element-red 'element-blue)
		:paths 'dogasus-pup)
)

(defun dogasus-wepawet ()  ; stage 3a: Pejustice (silver)
	(make-strain :name "Pejustice" :group 'dogasus
		:tags (list 'element-red 'element-blue)
		:paths 'dogasus-basic)
)

(defun dogasus-anubis ()  ; stage 3b: Pejustice (black)
	(make-strain :name "Pejustice" :group 'dogasus
		:tags (list 'element-red 'element-blue)
		:paths 'dogasus-basic)
)

(defun dogasus-carriage ()  ; stage 4: Ibizsedan
	(make-strain :name "Ibizsedan" :group 'dogasus
		:tags (list 'element-red 'element-blue)
		:paths (list 'dogasus-anubis 'dogasus-wepawet))
)

;; }}}

;; weewit / parhero / aphellion {{{

(defun killdeer ()
	(make-strain :name "Weewit"
		:tags (list 'element-blue)
		)
)

(defun killdeer-plover ()
	(make-strain :name "Weewit" :group 'killdeer
		:tags (list 'element-blue)
		)
)

(defun killdeer-parhelion ()
	(make-strain :name "Parhero" :group 'killdeer
		:tags (list 'element-red)
		)
)

(defun killdeer-aphelion ()
	(make-strain :name "Aphellion" :group 'killdeer
		:tags (list 'element-red)
		)
)

;; }}}

(register-species
	(list
		'faun-baby 'faun-strong 'faun-keeper 'faun-hero 'faun-spirit
		'killdeer-plover 'killdeer-parhelion 'killdeer-aphelion
		))

;;; }}}


;;; items {{{

;; portal items

(defun item-pear-common ()
	(make-item :name "Common Pear" :appeal 1))

(defun item-rock ()
	(make-item :name "Rock" :appeal -1))

;; Numbers are heptahedrons for capturing spaces_mons
(defun item-number ()
	(make-item :name "Number" :appeal 1 :recruit t
		))

(defun item-number-natural ()
	(make-item :name "Natural Number" :appeal 1 :recruit t
		))

(defun item-number-complex ()
	(make-item :name "Complex Number" :appeal 1 :recruit t
		))

(register-portal-item
	(list 'item-pear-common 'item-rock 'item-number 'item-number-natural 'item-number-complex))


;; other items

(defun item-pear-super ()
	(make-item :name "Super Pear" :appeal 1))

(defun item-pear-mythic ()
	(make-item :name "Mythic Pear" :appeal 1))


(defun item-number-imaginary ()
	(make-item :name "Imaginary Number" :appeal 1 :recruit t
		))

(defun item-number-infinite ()
	(make-item :name "Infinite Number" :appeal 1 :recruit t
		))


(defun item-activator-lead ()
	;; Heavy Activator allows Catrix and/or Ibizaor [to be added later] to evolve
	(make-item :name "Heavy Activator"
		:tags (list 'tag-activator)
		))

;;; }}}
