(defsystem "spaces_mons"
	:author "Valenoern"
	:licence "AGPL"
		;; shardquest and the existence of MMOs for existing monster games made me do it
	:description "Spaces_Mons sphere for shardquest"
	
	:depends-on ("shardquest")
	:components (
		(:file "shards")  ; :shardquest-spaces.shard
		(:file "spaces"   ; :shardquest-spaces
			:depends-on ("shards"))
))

;; this is just an attempt/demo and may or may not be integrated back into the zensekai engine when that gets started up again.
