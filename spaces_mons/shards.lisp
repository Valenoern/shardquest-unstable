(defpackage :shardquest-spaces.shard  ; type and shard related ideas
	(:shadowing-import-from :grevillea
		#:printl)
	(:shadowing-import-from :shardquest-item
		#:make-item)
	(:shadowing-import-from :shardquest-strain
		#:resident-stage-tags)
	(:use :common-lisp)
	(:export
		;; element element-shard
		#:element-red #:element-blue
		
		#:item-shard-red #:item-shard-blue
		
		#:resident-shards
))
(in-package :shardquest-spaces.shard)


;; elements/colours

(defstruct (element)  ; shard {{{
	;; this struct is pretty specific to the Spaces_Mons Sphere, so it will always be interacted with by Spaces_Mons related functions to produce something more 'standard'
	
	shard
	;; LATER: elements are going to do more stuff later, like calculate base stats and offer element-associated techniques. for now all they do is award shards
) ; }}}

(defun element-red ()
	(make-element
		:shard 'item-shard-red
))

(defun element-blue ()
	(make-element
		:shard 'item-shard-blue
))


;; shards

(defun item-shard-red ()
	(make-item :name "Red Shard"
		:tags (list 'tag-shard)
))

(defun item-shard-blue ()
	(make-item :name "Blue Shard"
		:tags (list 'tag-shard)
))


;; shard mechanics

(defun shard-quantity (shard-position) ; {{{
	'()
) ; }}}

(defun resident-shards (resident) ; decide what Shards, etc, to award for resident {{{
	(let (item-kinds
		(tags (resident-stage-tags resident))
	)
	(printl "RESIDENT SHARDS")
	
	;; for each strain tag, determine corresponding Shard/item
	(dolist (tag tags)
		(let* (
			(tag-data (funcall tag))
			(shard
				(if (element-p tag-data)
					(element-shard tag-data)
					nil))
			)
		(unless (null shard)
			(push shard item-kinds)
			)
	))
	
	tags  ; return list of Shard(s) without quantities
)) ; }}}

(setf (fdefinition 'shardquest-map:resident-shards) (function resident-shards))
