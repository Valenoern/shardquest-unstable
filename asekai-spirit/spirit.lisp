(defpackage :shardquest-asekai-spirit
	;; this is just a very crude test of registering monsters from an extension
	(:use :common-lisp)
	(:local-nicknames
		(:ext :shardquest-plugin)
		(:item :shardquest-map)  ; item-related stuff lives in here currently
	)
	(:export
		;; species
		#:spelaea-spelaea
		#:punk-punk
		#:croc-croc
		#:hokadou-hokadou
		
		;; portal items
		#:item-tea-valley
		#:item-fuel-methane
		#:item-soda-croc #:item-soda-spelaea
		
		;; other items
		#:item-signature-leader
))
(in-package :shardquest-asekai-spirit)


;;; species {{{

(defun spelaea-spelaea ()
	'()
)

(defun punk-punk ()
	'()
)

(defun croc-croc ()
	'()
)

(defun hokadou-hokadou ()
	'()
)

(ext:register-species
	(list 'spelaea-spelaea 'punk-punk 'hokadou-hokadou))

;;; }}}

;;; portal items {{{

(defun item-tea-valley () ; {{{
	(item:make-item :name "Valley Tea" :appeal 2)
	;; from Midday Valley, Nature sector
) ; }}}

(defun item-fuel-methane () ; {{{
	(item:make-item :name "Gas Tank" :appeal 2)
	;; 'we keep trying to convince them to switch to electric batteries but they won't listen'
) ; }}}

(defun item-soda-croc () ; {{{
	(item:make-item :name "Suchkyonade" :appeal 1)
	;; 'official sports drink of the A-06 Crocs, who will certainly defeat the Lions'
) ; }}}

(defun item-soda-spelaea () ; {{{
	(item:make-item :name "Lionade" :appeal 1)
	;; 'official sports drink of the L-70 Lions, who will certainly defeat the Crocs'
) ; }}}

(ext:register-portal-item
	(list 'item-tea-valley 'item-fuel-methane 'item-soda-croc 'item-soda-spelaea))

;;; }}}

;;; other items

(defun item-signature-leader () ; {{{
	(item:make-item :name "Calling Card" :appeal 1)
	;; an item for "catching" Kai, which you don't really do in asekai-zensekai, but in shardquest you always need an item to throw so you throw the infinite-use Calling Card.
) ; }}}
