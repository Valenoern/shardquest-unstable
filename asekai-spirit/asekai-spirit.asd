(defsystem "asekai-spirit"
	:author "Valenoern"
	:licence "AGPL"
		;; shardquest and the existence of MMOs for existing monster games made me do it
	:description "asekai sphere for shardquest - Spirit sector"
	
	:depends-on (
		"shardquest"
	)
	:components (
		(:file "spirit")  ; :shardquest-asekai-spirit
))

;; this is just an attempt/demo and may or may not be integrated back into the zensekai engine when that gets started up again.
