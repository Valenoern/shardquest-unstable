;;; item.lisp - inventory
(defpackage :shardquest-item ; {{
	(:shadowing-import-from :grevillea
		#:printl)
	(:shadowing-import-from :xylem
		#:xylem-value)
	(:use :common-lisp)
	(:local-nicknames
		(:x :xylem)
		(:s :shardquest-xylem)  ; namespace for xylem values
	)
	(:export
		;; item struct
		#:item #:make-item
			#:item-name #:item-appeal #:item-rarity #:item-recruit #:item-tags
			#:item-quantity
			#:item-with-rarity #:item-recruitp
		
		#:receive-item
		#:inventory
		
		#:item-shard
		
)) ; }}
(in-package :shardquest-item)


;;; item struct {{{

(defstruct (item) ; name appeal rarity recruit
	name
	appeal    ; how much this this item helps in winning over a monster
	rarity    ; drop rate for some situation, set in that situation
	recruit   ; if T, attempts to recruit a monster when used
	tags      ; categories which affect item's behaviour - Element, etc.
	
	quantity  ; count of item being passed - not always used
)

(defun item-with-rarity (item rarity)
	(let ((item
		(funcall item)))
	(setf (item-rarity item) rarity)
))

(defun item-recruitp (item)
	(not (null (item-recruit item)))
)

;;; }}}


(defun receive-item (item  &key (quantity 1)) ; put item into inventory {{{
	(let (
		(had (xylem-value (list 's:inventory item)))
	)
	(setf
		had
			(if (null had)
				1
				(+ had quantity))
		(xylem-value (list 's:inventory item))
			had)
	
	item  ; return item received
)) ; }}}


(defun inventory () ; describe inventory {{{
	(let (
		(item-list
			(xylem-value (list 'x:xylem-value 's:inventory)))
	)
	
	(format t "Inventory:~%")
	
	;; print quantities of each item
	(dolist (item-symbol item-list)
		(format t "~A × ~a~%"
			;; LATER: use gettext or something to localise item names
			(item-name (funcall item-symbol))
			(xylem-value (list 's:inventory item-symbol))
			))
	
	t
)) ; }}}


;;; basic shardquest items

(defun item-shard ()
	(make-item :name "Shard"
		:tags (list 'tag-shard)
		))
