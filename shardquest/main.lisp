;;; first demo for shardquest - currently only a REPL/console 'text adventure'
(defpackage :shardquest
	(:shadowing-import-from :shardquest-item
		#:inventory)
	(:shadowing-import-from :shardquest-ally
		#:choose-monster
		#:ally-list
		)
	(:shadowing-import-from :shardquest-map
		#:move-by #:move-x #:move-y
		#:visit-portal
		#:grid-status
		#:encounter-resident
		#:use-item
		)
	(:use :common-lisp)
	(:local-nicknames
		(:g :grevillea)
			;; printl debug-hash
		(:x :xylem)
		(:ext :shardquest-plugin)
		(:map :shardquest-map)
	)
	(:export
		#:*active-game*
		
		;; imported from map
		#:move-by #:move-x #:move-y
		#:visit-portal
		#:encounter-resident
		#:use-item
		#:choose-monster
		
		;; commands
		#:grid-status #:inventory #:ally-list
		
		#:begin
))
(in-package :shardquest)

;; attempt to track whether a game is open, for when loading system and stuff
;; LATER: I'd like to be able to begin the game and load the system at the same time, but I don't know if this is the best way
(defparameter *active-game* nil)


(defun begin () ; {{{
	(setq *active-game* t)
	(x:xylem-init)  ; initialise xylem table
	(let (dummy)
	dummy
	
	(g:load-extension "spaces_mons")
	
	(setq map:*grid* (map:random-grid))
	(grid-status)
	
	
	
	t
)) ; }}}
