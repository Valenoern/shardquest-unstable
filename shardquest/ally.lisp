;;; ally.lisp - residents/monsters in ally list
(defpackage :shardquest-ally ; {{
	(:shadowing-import-from :grevillea
		#:printl)
	(:shadowing-import-from :xylem
		#:xylem-value)
	(:use :common-lisp)
	(:local-nicknames
		(:x :xylem)
		(:s :shardquest-xylem)  ; namespace for xylem values
	)
	(:export
		#:receive-ally
		#:choose-monster
		#:ally-list
)) ; }}
(in-package :shardquest-ally)


(defun receive-ally (ally) ; put ally into list of recruited allies {{{
	;;(let (dummy)
	;;dummy
	
	(setf
		;; LATER: actually work with ally data structure, gensym is just temporary
		(xylem-value (list 's:ally-list (gensym)))  ally
		)
	
	ally  ; return ally received
) ; }}}

(defun choose-monster (options choice) ; submit choice of a series of monsters {{{
	(let (
		(allies
			(xylem-value (list 'x:xylem-value 's:ally-list)))
	)
	
	(cond
		((> (length allies) 1)
			(format t "You already have allies.~%")
		)
		((find choice options)  ; if option is in choices
			;; add ally to ally list
			(receive-ally choice)
			(format t "You received ~a!~%" choice)
				;; LATER: get real name of monster
		)
	)
	
	choice  ; return choice
)) ; }}}

(defun ally-list () ; describe allies {{{
	(let (
		(item-list
			(xylem-value (list 'x:xylem-value 's:ally-list)))
	)
	
	;; print info about each ally
	(dolist (ally-symbol item-list)
		(format t "Ally ~a - ~a~%"
			;; LATER: describe actual ally data structures when there are any
			ally-symbol
			(xylem-value (list 's:ally-list ally-symbol))
			)
	)
	
	t
)) ; }}}
