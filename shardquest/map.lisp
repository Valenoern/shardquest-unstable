;;; map.lisp - stuff related to travelling across map
(defpackage :shardquest-map ; {{{
	(:shadowing-import-from :grevillea
		#:printl)
	(:shadowing-import-from :xylem
		#:xylem-value)
	(:shadowing-import-from :shardquest-strain
		#:strain #:make-strain #:strain-p
		#:strain-group #:strain-name #:strain-tags #:strain-paths
		
		#:path #:make-path #:path-p
		#:path-rel #:path-name #:path-items
		
		#:resident #:make-resident #:resident-p
		#:resident-strain #:resident-stage #:resident-created
			#:resident-stage-name #:resident-stage-tags)
	(:shadowing-import-from :shardquest-item
		#:item  #:item-name #:item-appeal #:item-rarity #:item-recruit  #:make-item
			#:item-with-rarity #:item-recruitp
		#:receive-item)
	(:shadowing-import-from :shardquest-ally
		#:receive-ally
		)
	(:use :common-lisp)
	(:local-nicknames
		(:x :xylem)
			;; xylem-value
		(:s :shardquest-xylem)  ; namespace for xylem values
		
		(:dice :spaces_dice)
			;; random-item-weighted
		(:ext :shardquest-plugin)
	)
	(:export
		#:*grid* ; #::*grid-cell-size*
		
		;; structs
		#:portal  #:portal-name #:portal-used  #:make-portal
		#:cell  #:cell-width #:cell-portals #:cell-residents  #:make-cell
		#:grid  #:grid-width #:grid-xcell #:grid-ycell #:grid-cells  #:make-grid
			#:grid-active #:grid-cell #:grid-active-cell
		
		;; rolling portal items
		#:sample-portal-names
		#:portal-sine #:portal-sine-distribution
		#:roll-portal-bundle
		
		;; rolling random grid
		#:random-cell #:random-grid
		#:spawn-residents
		
		;; basic map interactions
		#:current-activity  ; #::*activity* #::*active-resident*
		#:move-by #:move-x #:move-y
		#:receive-ally
		#:visit-portal
		
		;; #::resident-shards-default
		#:resident-shards  ; can be redefined using 'fdefinition
		
		#:encounter-resident
		#:recruit-roll
		;; #::recruit-resident
		
		#:grid-status  ; #::cell-status #::encounter-status
		#:use-item  ; #::toss-encounter-item
		
)) ; }}}
(in-package :shardquest-map)

;; currently all of this is hardcoded to things appropriate for Spaces_Mons; eventually things like the portal roller will be customisable for other Spheres like asekai etc

;;; grid structs {{{

(defstruct (portal)  ; name {{{
	name   ; portal name
	(used 0)   ; time portal was last interacted with, default 0
) ; }}}

(defstruct (cell)  ; width portals population residents {{{
	width        ; width of cell in metres
	portals      ; list of portals 
	population   ; population density of cell, used for determining resident spawns
	residents    ; monsters currently on cell
) ; }}}


(defstruct (grid) ; width xcell ycell cells {{{
	width   ; number of cells on each side
	xcell   ; current x position
	ycell   ; current y position
	cells   ; array of cells
) ; }}}

(defun grid-active (grid) ; debug xcell / ycell - vector {{{
	(vector (grid-xcell grid) (grid-ycell grid))
) ; }}}

(defun grid-cell (x y grid) ; get cell from grid {{{
	(aref (grid-cells grid)
		(- (* x y) 1)
		)
) ; }}}

(defun grid-centre-cell (cell-size) ; calculate centre cell of grid {{{
	;; cell-size will usually be set with *grid-cell-size*, but, better coding style
	(-  (expt (/ cell-size 2) 2)  1)
) ; }}}

(defun grid-active-cell (grid) ; get currently active cell of grid {{{
	(grid-cell (grid-xcell grid) (grid-ycell grid) grid)
) ; }}}

;;; }}}

(defparameter *grid-cell-size* 16)
(defparameter *grid* nil)
(defparameter *activity* nil)  ; what activity is being done, affecting how commands behave

(defun current-activity (&key set-value) ; setter to change activity {{{
	(if (null set-value)
		*activity*  (setf *activity* set-value))
) ; }}}

(defparameter *active-resident* nil)  ; resident being examined in *activity*


;;; rolling portal items {{{

(defun sample-portal-names () ; {{{
	(list
		"Bench Number 50"
		"Seagull Statue"
		"Transforming Transformer"
		"Park Pavilion"
		"Greenbrown Coffee"
		"Matcha Maté!"
		"Steel Unicorn"
		"Roadside Park"
		"Storkflame Books"
		"Gateway of Fortitude"  ; this is for calling the cloud people
		"Nowhere Springs Post Office"
		"Iconic Bridge"
		"Tire Rim Turtle"  ; this actually exists and I made you look it up
		"Water Tower"
		"Row of Wagons"
		"The Pyramid"
		"North Dakota Beach"
)) ; }}}

(defun portal-sine (x) ; function for probabilities of each number of items {{{
	(if (< x 2)  ; split into a piecewise function at x = 2
		1  ; I couldn't quite get the function to be 1 at 1 but got tired of messing with it.
		
		;; y = (1/5) sin([16x + 7π] / 20) + 1.31 - (x + 9.1)² / 200
		(-
			(+
				(* (/ 1 5)  ; y = 1/5 ...
					(sin   ; sin([4/5]x + [7/20]π)
						(/ (+ (* 16 x)  (* 7 PI))  20)
						))
						;; PI is often provided as a system constant
				1.31)  ; + 1.31
		
			(/ (expt (+ x 9.1) 2)  200)  ; - (x + 9.1)² / 200
			)
		;; this looks complicated, but it's just a soft downwards "S".
	)
	
	;; the results will be about:
	;; 1 0.7795059287599626d0 0.5078761922485606d0 0.2687521418550396d0 0.1307534824884411d0 0.09509246848627706d0 0.09483891132202382d0
	
	;; LATER: see if this distribution is too generous by testing it in-game - if so archive this function & create a new one
) ; }}}

(defun portal-sine-distribution (&key precision) ; {{{
	(let (weights)
	;; the function to roll on a distribution uses ratios of integers
	(when (null precision)  (setq precision 10000))
	
	;; "integrate" the portal-sine on intervals of one
	(loop for i from 1 to 7 do
		(push  ; add calculation at this point to list of weights
			(floor (* (portal-sine i) 10000))
			weights))
	
	(reverse weights)
)) ; }}}

(defun roll-portal-bundle ()  ; roll list of items found at portal {{{
	(let (max-items items weights  dropped
		item-quantity  ; i could not say "number" because that's an item in Spaces_Mons
		(distribution (portal-sine-distribution))
	)
	
	(setq
		max-items 7
		items (ext:get-portal-item nil :all t)
		weights nil
		;; LATER: i am testing out simply picking from all registered items equally, put it back to rolling categories of those items later
		
		;;items (vector 'portal-tool-number 'portal-tool-heal 'portal-tool-appeal)
		;;weights (vector 4 1 2)
			;; 4 in 8 you obtain a Number
			;; 2 in 8 you obtain an appeal item for healing
			;; 1 in 8 you obtain an appeal item for catching
		;; LATER: actually choose particular items within these categories
	)
	
	(setq item-quantity  ; roll total number of items received {{{
		(dice:random-item-weighted  max-items :item-weights distribution  :index 1)
		) ; }}}
	
	;; roll each item
	(loop for i  from 1 to item-quantity  do
		(push
			(dice:random-item-weighted items :item-weights weights)
			dropped))
	
	dropped  ; return list of items in no particular order
)) ; }}}

;;; }}}

;;; rolling random grid {{{

(defun parabolic-distribution (x n) ; parabolic probability curve for getting n things {{{
	;; y = (x/N)^2 + 1
	(+
		(expt (/ x n) 2)
		1)
) ; }}}

(defun spawn-residents (cell) ; generate residents for cell {{{
	(let (quantity residents
		(possible-species (ext:get-species nil :all t))
		(population (cell-population cell))
	)
	(setq quantity  ; roll number of residents on cell {{{
		(dice:random-item-weighted
			population  :index 0  ; roll on the interval 0 ~ 'population
			:item-weights (lambda (x) (parabolic-distribution x population))
			)
	) ; }}}
	
	(loop for i  from 1 to quantity  do
		(let (species resident)
		(setq
			;; choose a random species from all known ones
			species (dice:random-item possible-species)
			resident
				(make-resident :stage species
					:created 0)
			)
		(push resident residents)
	))
	(setf (cell-residents cell) residents)
	
	cell  ; return cell with residents on it
)) ; }}}

(defun random-cell () ; generate a cell from scratch ; {{{
	(let (portal-quantity portals  population cell)
	
	(let ((max-portals (+ 1 3))  (distribution '(3 2 2 1))
	     )
		(setq portal-quantity
			(dice:random-item-weighted  max-portals :item-weights distribution  :index 0))
		)
	
	;; generate portals
	(let ((portal-names (sample-portal-names))) ; {{{
		(loop for i  from 1 to portal-quantity  do
			(push
				(make-portal :name (dice:random-item portal-names))
				portals)
	)) ; }}}
	
	;; base population will be proportional to number of portals and cell size
	(setq
		population
			(ceiling (* portal-quantity (expt (/ *grid-cell-size* 10) 2)))
		cell
			(spawn-residents
				(make-cell :width *grid-cell-size* :portals portals :population population))
		)
	
	cell
)) ; }}}

(defun random-grid () ; generate grid from scratch {{{
	(let (grid half
		(cell-quantity (expt *grid-cell-size* 2))
	)
	(setq grid (make-array cell-quantity))
	(setq half (/ *grid-cell-size* 2))
	
	(loop for i from 1 to cell-quantity do
		(setf (aref grid (- i 1)) (random-cell))
	)
	
	(make-grid
		:width *grid-cell-size* :xcell half :ycell half
		:cells grid)
)) ; }}}

;;; }}}


;;; basic map interactions {{{

(defun cell-status () ; describe grid cell {{{
	(let* ( ; let, in order:
		(residents (cell-residents (grid-active-cell *grid*)))
		(portals   (cell-portals (grid-active-cell *grid*)))
		
		(resident-quantity (length residents))
		(portal-quantity (length portals))
	)
	
	;; print portals on cell
	(format t "On this cell there are ~a portal(s).~%" portal-quantity)
	(loop for i  from 1 to portal-quantity  do
		(let (
			(portal (elt portals (- i 1)))
		)
		(format t "Portal ~a: ~a - used ~a~%"
			(- i 1)
			(portal-name portal)
			(portal-used portal)
			)
	))
	
	;; print residents on cell
	(unless (null residents)
		(format t "~%~a resident(s) appeared:~%" resident-quantity)
		(loop for i  from 1 to resident-quantity  do
			(let (
				(resident (elt residents (- i 1)))
			)
			(format t "~a: ~a~%"
				(- i 1)
				(resident-stage-name resident))
	)))
	
	;; we will wait until the game has a better interface to allow you to select portals/residents as anything but zero-indexed
	
	t
)) ; }}}

(defun move-by (x y) ; move to a new coordinate {{{
	(let ((grid *grid*))
	(let (
		(current-x (grid-xcell grid))  (current-y (grid-ycell grid))
	)
	
	(setf
		(grid-xcell grid) (+ x current-x)
		(grid-ycell grid) (+ y current-y)
		*grid* grid
		)
	
	;; attempt to describe current cell
	;; LATER: this is mainly for text mode. should the map screen just be a loop?
	(grid-status)
	
	(grid-active grid)  ; return current coordinate
)))

(defun move-x (&optional (x 1))
	;; move on x axis, by 1 unit if not specified
	(move-by x 0))

(defun move-y (&optional (y 1))
	;; move on y axis, by 1 unit if not specified
	(move-by 0 y))
;; }}}

(defun visit-portal (portal-id) ; interact with portal and get items {{{
	(let (portal portal-bundle
		(portals (cell-portals (grid-active-cell *grid*)))
	)
	
	(cond
		;; if portal list is empty or you're looking past the end of 'portals, stop
		((or  (null portals)  (> portal-id (- (length portals) 1)))
			(format t "There is no portal there.~%"))
		
		(t  ; if portal is in 'portals
			(setq portal (elt portals portal-id))
			
			;; LATER: only give items every 3 minutes
			portal  ; leave unused var alone
			(setq portal-bundle (roll-portal-bundle))
			
			;; you should have gotten at least 1 item but just in case
			(unless (null portal-bundle)
				(format t "You received ~a item(s):~%" (length portal-bundle))
				)
		
			(dolist (item portal-bundle)
				(receive-item item)
				;; print "item name"
				;; LATER: use gettext or something to localise item names
				(format t "~a~%"
					(item-name (funcall item))
					))
	))
	
	portal-bundle  ; return items received from portal
)) ; }}}

;;; }}}

;;; shards per resident {{{

(defun resident-shards-default (resident) ; decide what Shards, etc, to award for resident {{{
	(let (item-kinds
		(tags (resident-stage-tags resident))
	)
	
	(printl "RESIDENT TAGS" tags)
	
	
	
	
	;; LATER: allow each Sphere to define its own shard algorithm
)) ; }}}

(defparameter resident-shards nil)
(setf (fdefinition 'resident-shards) (function resident-shards-default))

;;; }}}

;;; encounter {{{

(defun encounter-status () ; {{{
	(let (
		(resident *active-resident*)
	)
	
	(format t "Currently facing:~%~a~%"
		(resident-stage-name resident))
	
	resident
)) ; }}}

(defun encounter-resident (resident) ; {{{
	(let (to-encounter
		(residents (cell-residents (grid-active-cell *grid*)))
	)
	(cond
		((equal *activity* 's:encounter-resident)
			(encounter-status))
		
		((or  (null residents)  (> resident (- (length residents) 1)))
			(format t "There is no resident there.~%")
			nil)  ; return no resident
		
		(t
			(current-activity :set-value 's:encounter-resident)
			(setq
				to-encounter (elt residents resident)
				*active-resident* to-encounter
				)
			
			(encounter-status)
			
			to-encounter  ; return resident
	))
)) ; }}}

(defun recruit-roll (resident) ; roll to recruit resident - placeholder {{{
	(let (roll)
	
	;; LATER: create actual recruit rates
	(setq roll (dice:random-integer 1 3))
	(if (< roll 2)
		nil
		t)
	
	;; LATER: there can be one function for different kinds of recruiting including with item and otherwise
)) ; }}}

(defun recruit-resident (resident) ; {{{
	(let* (
		(resident-kind (resident-stage-name resident))
		(roll (recruit-roll resident))
		;; LATER: create actual recruit rates
	)
	
	(cond
		((null roll)
			(format t "Missed."))
		(t
			(format t "Successfully caught ~a.~%" resident-kind)
			(receive-ally resident)
			;; recieve Shards
			(resident-shards resident)
			(receive-item 'shardquest-item:item-shard :quantity 1)
			))
	
	resident
)) ; }}}

(defun toss-encounter-item (item) ; {{{
	(let* (
		(item-data (funcall item))
		(resident *active-resident*)
		(resident-kind (resident-stage-name resident))
	)
	;; #S(SHARDQUEST-STRAIN:RESIDENT :STAGE SHARDQUEST-SPACES:FAUN-HERO :CREATED 0)
	
	(format t "You throw ~a at ~a——~%"
		(item-name item-data)  resident-kind)
	
	(cond
		;; if this item is for recruiting/catching, attempt to recruit resident
		((item-recruitp item-data)
			(recruit-resident resident))
	)
	
	item  ; return item
)) ; }}}

;;; }}}


;;; "generic" commands {{{
;; I have decided I'll only use CLOS where it's absolutely necessary, like maybe for graphical vs text interface. for simple things like which screen this is, a regular variable will do

(defun grid-status ()
	(cond
		((equal *activity* 's:encounter-resident)
			(encounter-status))
		(t  (cell-status))
))

(defun use-item (item)
	(cond
		((equal *activity* 's:encounter-resident)
			(toss-encounter-item item))
))

;;; }}}
