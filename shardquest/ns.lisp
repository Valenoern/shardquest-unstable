;;; ns.lisp - this package does nothing but export symbols to use as xylem keys
(defpackage :shardquest-xylem
	(:use :common-lisp)
	(:export
		#:encounter-resident  ; used during 'encounter-resident
		
		#:ally-list  ; used for receiving/describing allies
		#:inventory  ; used for receiving/describing inventory items
))
