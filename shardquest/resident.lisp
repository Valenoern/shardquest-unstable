;;; resident.lisp - stuff related to monsters, aka residents/allies
;; some of this is copied from zensekai-keychain experiment,
;; which is likely to be superseded by and then merge with shardquest
(defpackage :shardquest-strain ; {{{
	(:use :common-lisp)
	(:export
		#:strain #:make-strain #:strain-p
		#:strain-group #:strain-name #:strain-tags #:strain-paths
		
		#:path #:make-path #:path-p
		#:path-rel #:path-name #:path-items
		
		#:resident #:make-resident #:resident-p
		#:resident-strain #:resident-stage #:resident-created
			#:resident-stage-name #:resident-stage-tags
		
)) ; }}}
(in-package :shardquest-strain)


(defstruct strain
	;; data for either species/strain or stage/form
	group    ; parent strain, if this is a form
	name     ; natural-language name
	tags     ; categories form/strain belongs to, which may affect behaviour
	paths    ; methods for reaching this form
)

(defstruct path
	rel      ; specific kind of meta path - cf. link[rel], bopwiki meta line :rel
	name     ; natural-language name
	items    ; items required to meta, such as shards
)

(defstruct resident
	stage    ; stage or form plus strain - here they're combined
	created  ; part of stat generation seed
)

;;; deep accessors

(defun resident-stage-name (resident) ; friendly name of resident's form
	(strain-name (funcall (resident-stage resident)))
)

(defun resident-stage-tags (resident) ; categories of resident's form
	(strain-tags (funcall (resident-stage resident)))
)
